---
title: Disruption of Metrics website and relay search
date: 2021-06-03 08:00:00
resolved: true
resolvedWhen: 2021-06-14 16:00:00
# Possible severity levels: down, disrupted, notice
severity: disrupted
affected:
  - metrics.torproject.org
section: issue
---

We're currently facing stability issues with respect to our Metrics website and relay search. We are actively [working on resolving those issues](https://gitlab.torproject.org/tpo/metrics/team/-/issues/15)
as soon as possible.
