---
title: Tor Weather outage
date: 2023-04-04 21:00:00 +0000
resolved: true
resolvedWhen: 2023-04-12 15:46:00 +0000
# Possible severity levels: down, disrupted, notice
severity: down
affected:
  - weather.torproject.org
section: issue
---

We needed to take down Tor Weather temporarily as it was [spamming the
tor-commits mailing list](https://gitlab.torproject.org/tpo/tpa/team/-/issues/41118). Once we have an acceptable solution to that problem it will be re-enabled. Sorry for the inconvenience.
