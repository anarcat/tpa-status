---
title: Mailman 3 upgrade
date: 2024-11-04 14:00:00 +0000
resolved: true
resolvedWhen: 2024-11-04 21:00:00 +0000
# Possible severity levels: down, disrupted, notice
severity: notice
affected:
  - Mailing lists
section: issue
---

TL;DR: Mailman 3 upgrade happened on November 4th, starting at 14:00
UTC, and ending around 21:00 UTC. Report issues on GitLab.

As mentioned in early October, we're in the process of upgrading our
main mail server, which includes upgrading to the shiny new Mailman 3
platform. This will involve short per-list outage as each migrates
over to the new server. It's unclear how long the maintenance will
last, but assume mailing lists will be disrupted all day, although
each migration should only take a couple of minutes each.

We have, right now, a prototype mailman 3 server available at:

https://lists-01.torproject.org/

The TPA mailing list has been succesfully migrated already and next
week, I'll start migrating the other mailing lists (including this
one!).

Be warned that Mailman 3 is a significant upgrade from Mailman 2. There
are some great things (like unified authentication), and some less great
things (like a more complex design and "shinier" web interface that
might not be everyone's taste).

As a reminder, we're doing this upgrade a little rushed because the main
mail server is now unsupported for security upgrades. See the details of
the proposal here:

https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy/tpa-rfc-71-emergency-email-deployments-round-2

... with the upgrade work being tracked in this issue:

<https://gitlab.torproject.org/tpo/tpa/team/-/issues/40471>
